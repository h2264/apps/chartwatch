"""
GitHandler
"""

from __future__ import annotations

import logging
import subprocess
from pathlib import Path
from typing import Any

import yaml


class GitHandler:
    """
    Handles Git operations such as cloning a repository and managing app files.
    """

    def __init__(
        self: GitHandler,
        _repo_name: str,
        _url: str,
        _path: str,
        _app_files: list[str],
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialize the GitHandler with repository details and optional logger.

        Args:
            _repo_name (str): The name of the repository.
            _url (str): The URL of the Git repository.
            _path (str): The local path where the repository will be cloned.
            _app_files (list[str]): List of relative paths to YAML files of interest.
            logger (logging.Logger | None): Optional logger for logging messages.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.repo_name = _repo_name
        self.repo_url = _url
        self.repo_path = Path(_path) / _repo_name  # Correct path construction
        self.app_files = _app_files
        self.apps: dict[str, Any] = {}
        self.clone_repository()

    def clone_repository(self: GitHandler) -> None:
        """
        Clone the Git repository to the specified path.
        If the path does not exist, create it.
        """
        if not self.repo_path.exists():
            self.logger.info(f"Creating directory {self.repo_path}")
            self.repo_path.mkdir(parents=True, exist_ok=True)

        clone_commands = ["git", "clone", self.repo_url, str(self.repo_path), "--recursive"]
        self.logger.info(f"Cloning repository with commands: {clone_commands}")
        try:
            subprocess.run(clone_commands, check=True)
            self.logger.info(f"Repository cloned successfully to {self.repo_path}")
            self.display_repository_files()
            self.load_app_files()
        except subprocess.CalledProcessError as e:
            self.logger.error(f"Failed to clone git repository: {e}")

    def pull_repository(self: GitHandler) -> None:
        """
        Update the local Git repository by pulling the latest changes with rebase.
        Executes a `git pull --rebase` command in the repository directory. Logs
        success or failure and reloads application files upon successful execution
        """
        repo_path = str(self.repo_path)
        pull_commands = ["git", "pull", "--rebase"]
        self.logger.info(f"Updating repository [{repo_path}] with commands: {pull_commands}")
        try:
            subprocess.run(pull_commands, check=True, cwd=repo_path)
            self.logger.info(f"Updated git repository '{repo_path}'")
            self.load_app_files()
        except subprocess.CalledProcessError as e:
            self.logger.error(f"Failed to pull git repository: {e}")

    def delete_repository(self: GitHandler) -> None:
        """
        TBA
        """
        repo_path = str(self.repo_path)
        delete_commands = ["rm", "-rf", repo_path]
        self.logger.info(f"Deleting repository [{repo_path}] with commands: {delete_commands}")
        try:
            subprocess.run(delete_commands, check=True)
            self.logger.info(f"Deleted git repository '{repo_path}'")
        except subprocess.CalledProcessError as e:
            self.logger.error(f"Failed to delete git repository: {e}")

    def display_repository_files(self: GitHandler) -> None:
        """
        Display the files in the cloned repository.
        """
        try:
            files = list(self.repo_path.rglob("*"))  # Recursively get all files and directories
            self.logger.debug(f"Files in the cloned repository ({self.repo_path}):")
            for file in files:
                self.logger.debug(file)
        except OSError as e:
            self.logger.error(f"Failed to list files in {self.repo_path}: {e}")
            raise  # Re-raise to signal file listing failure

    def load_app_files(self: GitHandler) -> None:
        """
        Load YAML files of interest into self.apps.

        The relative path to each file becomes the key in the self.apps dictionary,
        and the file's contents become the value.
        """
        self.logger.info(f"Loading application files: {self.app_files}")
        for relative_path in self.app_files:
            yaml_path = self.repo_path / relative_path  # Construct full path
            self.logger.debug(f"yaml_path: {yaml_path}")

            try:
                with yaml_path.open("r") as yaml_file:
                    content = yaml.safe_load(yaml_file)  # Load YAML content
                    self.apps[relative_path] = content["apps"]
            except yaml.YAMLError:
                self.logger.error(f"Failed to parse YAML file {yaml_path}")
            except OSError as e:
                self.logger.error(f"Failed to read file {yaml_path}: {e}")

        self.logger.debug(f"self.apps: {self.apps}")
        self.logger.info(f"Loaded {len(self.apps)} application files successfully.")
