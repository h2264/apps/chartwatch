"""
ChartHandler
"""

from __future__ import annotations

import logging
import subprocess
from typing import Any

import yaml


class ChartHandler:
    """
    Handles Helm chart operations such as managing repositories, fetching chart versions,
    and synchronizing local and remote chart configurations.
    """

    def __init__(self: ChartHandler, _app_file: dict[Any], logger: logging.Logger | None = None) -> None:
        """
        Initializes the ChartHandler instance.

        Args:
            _app_file (dict[Any]): A dictionary containing app configurations.
            logger (logging.Logger, optional): Logger instance for logging messages.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.charts = {}
        self.remote_charts = {}
        self.load_apps(_app_file=_app_file)
        self.logger.debug(f"self.charts: {self.charts}")

    def load_apps(self: ChartHandler, _app_file: dict[Any]) -> None:
        """
        Loads applications from the provided app configuration file.

        Args:
            _app_file (dict[Any]): A dictionary of application configurations.
        """
        self.logger.debug(f"_app_file: {_app_file}")
        for app, app_config in _app_file.items():
            try:
                if app_config["installType"] == "helm":
                    self.logger.info(f"Application {app} detected with helm installType")
                    self.charts[app] = app_config
            except Exception as e:
                self.logger.error(f"Failed to parse {app} configuration: {e}")

    def add_chart_repos(self: ChartHandler) -> None:
        """
        Adds Helm chart repositories based on the loaded app configurations.
        """
        for app, app_config in self.charts.items():
            self.add_chart_repo(_repo_name=app_config["chartName"], _repo_url=app_config["repoURL"])
            self.logger.info(f"Chart repository {app} added")

    def add_chart_repo(self: ChartHandler, _repo_name: str, _repo_url) -> None:
        """
        Adds Helm chart repository based on the loaded app configurations.
        """
        helm_command = ["helm", "repo", "add", _repo_name, _repo_url]
        self.chart_command_executor(helm_command=helm_command)

    def update_chart_repos(self: ChartHandler) -> None:
        """
        Updates Helm chart repositories for all loaded apps.
        """
        for app, app_config in self.charts.items():
            self.update_chart_repo(_repo_name=app_config["chartName"], _repo_url=app_config["repoURL"])
            self.logger.info(f"Chart repository {app} updated")

    def update_chart_repo(self: ChartHandler, _repo_name: str, _repo_url) -> None:
        """
        Updates Helm chart repository based on the loaded app configurations.
        """
        helm_command = ["helm", "repo", "update", _repo_name]
        repo_found, correct_url = self.check_helm_repo_details(_repo_name=_repo_name, _repo_url=_repo_url)
        if repo_found:
            if correct_url:
                self.chart_command_executor(helm_command=helm_command)
            else:
                self.logger.warning(f"Deleting repository {_repo_name} due to incorrect url")
                self.delete_chart_repo(_repo_name=_repo_name)
                self.logger.warning(f"Adding deleted repository {_repo_name} with correct url {_repo_url}")
                self.add_chart_repo(_repo_name=_repo_name, _repo_url=_repo_url)
        else:
            self.logger.warning(f"Adding missing repository {_repo_name} with correct url {_repo_url}")
            self.add_chart_repo(_repo_name=_repo_name, _repo_url=_repo_url)

    def delete_chart_repos(self: ChartHandler) -> None:
        """
        Deletes Helm chart repositories for all loaded apps.
        """
        for app, app_config in self.charts.items():
            self.delete_chart_repo(_repo_name=app_config["chartName"])
            self.logger.info(f"Chart repository {app} deleted")

    def delete_chart_repo(self: ChartHandler, _repo_name: str) -> None:
        """
        Deletes Helm chart repository for loaded apps.
        """
        helm_command = ["helm", "repo", "remove", _repo_name]
        self.chart_command_executor(helm_command=helm_command)
        self.logger.info(f"Chart repository {_repo_name} deleted")

    def chart_command_executor(self: ChartHandler, helm_command: list) -> None:
        """Execute a Helm command and log success or failure."""
        self.logger.debug(f"helm_command: {helm_command}")
        try:
            subprocess.run(helm_command, check=True)
            self.logger.debug(f"Helm command {helm_command} successful")
        except Exception as e:
            self.logger.error(f"Failed to execute helm command {helm_command}: {e}")

    def check_helm_repo_details(self: ChartHandler, _repo_name: str, _repo_url: str) -> tuple[bool, bool]:
        """Verify if a Helm repository exists and has the correct URL."""
        repo_found = False
        url_correct = False
        helm_command = ["helm", "repo", "list", "-o", "json"]
        try:
            result = subprocess.run(helm_command, capture_output=True, text=True, check=True)
            chart = yaml.safe_load(result.stdout)
            repos = {item["name"]: item["url"] for item in chart}
            if _repo_name in repos.keys():
                repo_found = True
                if repos[_repo_name] == _repo_url:
                    url_correct = True
                else:
                    self.logger.warning(
                        f"Chart repository url incorrect. Expected={_repo_url} Actual=[{repos[_repo_name]}]"
                    )
        except Exception as e:
            self.logger.error(f"Failed to interrogate repository with command {helm_command}: {e}")
        if not repo_found:
            self.logger.warning(f"Chart repository {_repo_name} not found")
        return repo_found, url_correct

    def get_remote_apps(self: ChartHandler, _chart_name: str) -> None:
        """
        Fetches information about remote Helm charts for all loaded apps.
        """
        helm_command = ["helm", "search", "repo", f"{_chart_name}/{_chart_name}", "-o", "json"]
        self.logger.debug(f"helm_command: {helm_command}")
        try:
            result = subprocess.run(helm_command, capture_output=True, text=True, check=True)
            chart = yaml.safe_load(result.stdout)
            self.remote_charts[_chart_name] = chart[0]
            self.logger.info(f"Chart {_chart_name}/{_chart_name} found and loaded")
            self.logger.debug(chart)
        except Exception as e:
            self.logger.error(f"Error encountered for loading remote chart [{_chart_name}/{_chart_name}]: {e}")
        self.logger.debug(f"self.remote_charts: {self.remote_charts}")

    def get_local_charts(self: ChartHandler, chart_name: str) -> dict[str, Any]:
        """
        Retrieves the local details of the specified Helm chart.

        Args:
            chart_name (str): Name of the chart.

        Returns:
            dict[str, Any]: The local chart configuration.
        """
        _chart = None
        try:
            # Transform the dictionary
            _chart = self.charts[chart_name]
        except Exception:
            self.logger.error(f"Failed to load {chart_name} from local charts: {self.charts.keys()}")
        return _chart

    def get_remote_charts(self: ChartHandler, chart_name: str) -> dict[str, Any]:
        """
        Retrieves the remote version of the specified Helm chart.

        Args:
            chart_name (str): Name of the chart.

        Returns:
            dict[str, Any]: The remote chart configuration.
        """
        _chart = None
        self.logger.debug("Updating local cache for remote charts")
        self.remote_charts[chart_name] = {}
        self.get_remote_apps(_chart_name=chart_name)
        return self.remote_charts[chart_name]

    def get_chart_details(self: ChartHandler, chart_name: str) -> tuple[dict[str, Any], dict[str, Any]]:
        """
        Retrieves both local and remote details of the specified Helm chart.

        Args:
            chart_name (str): Name of the chart.

        Returns:
            tuple[dict[str, Any], dict[str, Any]]: A tuple containing the local and remote chart configurations.
        """
        local_chart = self.get_local_charts(chart_name=chart_name)
        remote_chart = self.get_remote_charts(chart_name=chart_name)
        self.logger.debug(f"local_chart: {local_chart}")
        self.logger.debug(f"remote_chart: {remote_chart}")
        return local_chart, remote_chart

    def compare_version(self: ChartHandler, chart_name: str) -> tuple[str, str, bool]:
        """Interrogate local and remote chart versions."""
        outdated = False
        local_chart = self.get_local_charts(chart_name=chart_name)
        remote_chart = self.get_remote_charts(chart_name=self.charts[chart_name]["chartName"])
        try:
            if local_chart["targetRevision"] != remote_chart["version"]:
                outdated = True
            return local_chart["targetRevision"], remote_chart["version"], outdated
        except Exception as e:
            self.logger.error(f"Failed to fetch versions: {e}")
            return "", "", None
