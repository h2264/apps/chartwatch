"""
Notifications module utilising slack python SDK.
"""

from __future__ import annotations

import logging
from os import environ

from slack_sdk.webhook import WebhookClient

from .engine import NotificationsEngine

ENV_VARS = ["SLACK_WEBHOOK"]


class SlackEngine(NotificationsEngine):
    """Concrete class for the Slack Notification Engine."""

    def __init__(self: SlackEngine, logger: logging.Logger | None = None) -> None:
        """
        Create a new SlackEngine object. Loads configuration form environment variables.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.config = {}
        self.load_config()
        self.webhook = WebhookClient(self.config["SLACK_WEBHOOK"])

    def load_config(self: SlackEngine) -> None:
        """
        Load SlackEngine configurations.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        try:
            for env_var in ENV_VARS:
                self.config[env_var] = environ[env_var]
            self.logger.debug(f"loaded_vars: {self.config}")
        except Exception as e:
            self.logger.error(f"Failed to environment variables. Error: {e}")

    def validate_config(self: SlackEngine) -> None:
        """
        Validate SlackEngine configurations.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        response = self.webhook.send(text="SlackEngine validation message")
        assert response.status_code == 200
        assert response.body == "ok"

    def send_notifications(self: SlackEngine, message: str) -> None:
        """
        Send notification using SlackEngine.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        response = self.webhook.send(text=message)
        assert response.status_code == 200
        assert response.body == "ok"
