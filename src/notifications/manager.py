"""
Module for managing notifications engines.
"""

from __future__ import annotations

import logging

from notifications import SlackEngine


class NotificationsManager:
    """Concrete class for the managing Notification Engines."""

    def __init__(self: NotificationsManager, logger: logging.Logger | None = None) -> None:
        """
        Create a new NotificationsManager object.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.engines = {}
        self.init_engines()

    def init_engines(self: NotificationsManager, logger: logging.Logger | None = None) -> None:
        """
        Create a new notificationsEngine object.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        self.engines = {"slack": SlackEngine(logger=logger)}

    def send_messages(self: NotificationsManager, message: str) -> None:
        """
        Create a new notificationsEngine object.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        self.logger.debug(f"message: {message}")
        for engine in self.engines:
            engine.send_notifications(message)
