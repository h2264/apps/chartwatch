"""
Abstract class for notification engines.
"""

from __future__ import annotations

import logging
from abc import ABC, abstractmethod


class NotificationsEngine(ABC):
    """Abstract class for managing Notification Engines."""

    @abstractmethod
    def __init__(self: NotificationsEngine, logger: logging.Logger | None = None) -> None:
        """
        Create a new NotificationsEngine object.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        raise NotImplementedError

    @abstractmethod
    def load_config(self: NotificationsEngine) -> None:
        """
        Load NotificationsEngine configurations.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        raise NotImplementedError

    @abstractmethod
    def validate_config(self: NotificationsEngine) -> None:
        """
        Validate NotificationsEngine configurations.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        raise NotImplementedError

    @abstractmethod
    def send_notifications(self: NotificationsEngine, message: str) -> None:
        """
        Send notification using NotificationsEngine.
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        raise NotImplementedError
