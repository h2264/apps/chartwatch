"""
Module initialisation for the notifications module of chartwatch

Author: Jesmigel Cantos
"""

from .slack import SlackEngine

__all__ = ["SlackEngine"]
