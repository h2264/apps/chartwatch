"""
GitHandler test harness
"""

import logging
import subprocess
from pathlib import Path
from unittest.mock import Mock, call, mock_open, patch

import pytest
import yaml

from handlers.git import GitHandler


# Fixtures for parameterized tests
@pytest.fixture
def mock_logger():
    """
    Fixture to mock a logger for testing.
    Returns a mock logging.Logger object.
    """
    return Mock(spec=logging.Logger)


@pytest.fixture(params=[("https", "https://github.com/example/repo.git"), ("ssh", "git@github.com:example/repo.git")])
def git_url(request):
    """
    Fixture to provide different Git URL protocols (HTTPS or SSH).
    This fixture is parameterized to test both URL types.
    """
    return request.param


@pytest.fixture(
    params=[
        (
            ["config/app1.yaml", "config/app2.yaml"],
            {
                "config/app1.yaml": {"apps": {"app1": {"key": "value"}}},
                "config/app2.yaml": {"apps": {"app1": {"key": "value"}}},
            },
        ),
        ([], {}),
        (["config/app1.yaml"], {"config/app1.yaml": {"apps": {"app1": {"key": "value"}}}}),
    ]
)
def app_files(request):
    """
    Fixture to provide different sets of application files (with or without YAML files).
    Each test will receive a tuple of file paths and their corresponding YAML content (if any).
    """
    return request.param


@pytest.fixture(params=[("/tmp/repo", True), ("/tmp/non_existing_repo", False)])
def repo_path(request):
    """
    Fixture to simulate an existing or non-existing repository path.
    Used to test path-related behaviors, such as cloning a repository or creating a new path.
    """
    return request.param


@pytest.fixture
def mock_subprocess():
    """
    Fixture to mock subprocess.run for testing Git commands.
    Mocks the subprocess.run function used for cloning repositories.
    """
    with patch("subprocess.run") as mock_run:
        yield mock_run


@pytest.fixture
def mock_path():
    """
    Fixture to mock pathlib.Path methods (exists and mkdir).
    Simulates checking for the existence of directories and creating directories as needed.
    """
    with patch("pathlib.Path.exists") as mock_exists, patch("pathlib.Path.mkdir") as mock_mkdir:
        yield mock_exists, mock_mkdir


# Test cases
def test_clone_repository_success(mock_subprocess, mock_logger, git_url):
    """
    Test case to verify the successful cloning of a Git repository using subprocess.run.
    Verifies that subprocess.run is called with the correct arguments and that the success message is logged.
    """
    mock_subprocess.return_value = Mock(returncode=0)
    GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Ensure subprocess.run was called with correct clone command
    mock_subprocess.assert_called_once_with(
        ["git", "clone", git_url[1], str(Path("/tmp") / "repo_name"), "--recursive"], check=True
    )
    mock_logger.info.assert_any_call(f"Repository cloned successfully to {Path('/tmp') / 'repo_name'}")


def test_clone_repository_failure(mock_subprocess, mock_logger, git_url, mock_path):
    """
    Test failure handling during repository cloning.
    Simulates a subprocess.CalledProcessError and verifies that the error is logged correctly.
    """
    # Mock subprocess.run to simulate a CalledProcessError
    mock_subprocess.side_effect = subprocess.CalledProcessError(1, "git clone")

    # Mock repo_path existence and directory creation
    mock_exists, _ = mock_path
    mock_exists.return_value = True  # Simulate the path already exists

    GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Assert that the error was logged correctly
    mock_logger.error.assert_called_once_with(
        "Failed to clone git repository: Command 'git clone' returned non-zero exit status 1."
    )

    # Ensure subprocess.run was called with expected arguments
    mock_subprocess.assert_called_once_with(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)


def test_pull_repository_success(mock_subprocess, mock_logger, git_url, mock_path):
    """
    Test successful repository update using the 'pull_repository' method.
    Verifies that the subprocess runs without errors and logs the success message.
    """
    # Mock repo_path existence
    mock_exists, _ = mock_path
    mock_exists.return_value = True  # Simulate the path already exists

    handler = GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Execute the method
    handler.pull_repository()
    print(git_url)

    # Assert subprocess.run was called with expected arguments
    mock_subprocess.assert_has_calls(
        [
            call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True),
            call(["git", "pull", "--rebase"], check=True, cwd="/tmp/repo_name"),
        ]
    )

    # Assert the success message was logged
    mock_logger.info.assert_any_call("Updated git repository '/tmp/repo_name'")


def test_delete_repository_success(mock_subprocess, mock_logger, git_url, mock_path):
    """
    Test case to verify successful deletion of a Git repository using subprocess.run.
    Verifies that subprocess.run is called with the correct arguments and that the success message is logged.
    """
    # Mock repo_path existence
    mock_exists, _ = mock_path
    mock_exists.return_value = True  # Simulate the path already exists

    # Mock subprocess.run to simulate successful execution
    mock_subprocess.return_value = Mock(returncode=0)

    handler = GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Execute the method
    handler.delete_repository()

    # Assert subprocess.run was called with expected arguments
    mock_subprocess.assert_has_calls(
        [
            call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True),
            call(["rm", "-rf", "/tmp/repo_name"], check=True),
        ]
    )

    # Ensure success log was captured
    mock_logger.info.assert_any_call(
        "Deleting repository [/tmp/repo_name] with commands: ['rm', '-rf', '/tmp/repo_name']"
    )
    mock_logger.info.assert_any_call("Deleted git repository '/tmp/repo_name'")


def test_delete_repository_failure(mock_subprocess, mock_logger, git_url, mock_path):
    """
    Test failure handling during repository deletion.
    Simulates a subprocess.CalledProcessError and verifies that the error is logged correctly.
    """
    # Mock repo_path existence
    mock_exists, _ = mock_path
    mock_exists.return_value = True  # Simulate the path already exists

    # Mock subprocess.run to simulate a CalledProcessError
    mock_subprocess.side_effect = subprocess.CalledProcessError(1, "rm -rf")

    handler = GitHandler("repo_name", git_url[1], "/tmp", [], logger=mock_logger)

    # Execute the method
    handler.delete_repository()

    # Assert subprocess.run was called with expected arguments
    mock_subprocess.assert_has_calls(
        [
            call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True),
            call(["rm", "-rf", "/tmp/repo_name"], check=True),
        ]
    )

    # Assert that the error was logged correctly
    mock_logger.error.assert_any_call(
        "Failed to delete git repository: Command 'rm -rf' returned non-zero exit status 1."
    )


def test_pull_repository_failure(mock_subprocess, mock_logger, git_url, mock_path):
    """
    Test failure handling during repository update using the 'pull_repository' method.
    Simulates a subprocess.CalledProcessError and verifies that the error is logged correctly.
    """
    mock_subprocess.side_effect = subprocess.CalledProcessError(1, "git pull")

    # Mock repo_path existence
    mock_exists, _ = mock_path
    mock_exists.return_value = True  # Simulate the path already exists

    handler = GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Execute the method
    handler.pull_repository()

    # Assert subprocess.run was called with expected arguments
    mock_subprocess.assert_has_calls(
        [
            call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True),
            call(["git", "pull", "--rebase"], check=True, cwd="/tmp/repo_name"),
        ]
    )

    # Assert the error message was logged
    mock_logger.error.assert_any_call(
        "Failed to pull git repository: Command 'git pull' returned non-zero exit status 1."
    )


def test_path_creation(mock_subprocess, mock_path, mock_logger, git_url, repo_path):
    """
    Test case to verify the correct behavior when creating a repository path.
    Ensures that the directory is created when the path does not exist and
    that subprocess.run is called with the correct arguments.
    """
    mock_exists, mock_mkdir = mock_path
    mock_exists.return_value = repo_path[1]

    GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    # Assert the subprocess call
    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    # Should not call mkdir if the repo path exists
    if repo_path[1]:
        mock_mkdir.assert_not_called()
    else:
        mock_mkdir.assert_called_once_with(parents=True, exist_ok=True)


def test_display_repository_files(mock_subprocess, git_url, mock_logger):
    """
    Test case to verify that files in a cloned repository are displayed correctly.
    Ensures that the logger logs the file paths in the cloned repository after
    calling the display_repository_files method.
    """
    handler = GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    with patch("pathlib.Path.rglob") as mock_rglob:
        mock_rglob.return_value = [Path("/tmp/repo_name/config/app1.yaml"), Path("/tmp/repo_name/config/app2.yaml")]

        handler.display_repository_files()
        mock_rglob.assert_called_once_with("*")


def test_display_repository_files_failure(mock_subprocess, git_url, mock_logger):
    """
    Test case to simulate a failure when attempting to list files in the cloned repository.
    An OSError is simulated and the method is expected to raise an exception and log the error message.
    """
    handler = GitHandler("repo_name", git_url[1], "/tmp", ["config/app1.yaml"], logger=mock_logger)

    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    with patch("pathlib.Path.rglob") as mock_rglob:
        # Simulate an OSError when calling rglob
        mock_rglob.side_effect = OSError("Permission denied")

        # Call the method under test, which should raise the OSError and log the error
        with pytest.raises(OSError, match="Permission denied"):
            handler.display_repository_files()

        mock_rglob.assert_called_once_with("*")

        # Ensure the error was logged with the expected message
        mock_logger.error.assert_any_call("Failed to list files in /tmp/repo_name: Permission denied")


def test_load_app_files_success(mock_subprocess, git_url, mock_logger, app_files, mock_path):
    """
    Test case to verify the successful loading of application files (YAML).
    Simulates reading valid YAML files and ensures that the files are loaded
    and the data is correctly stored in the handler's apps attribute.
    """
    file_paths, expected_apps = app_files

    # Initialize the handler with mock data
    handler = GitHandler("repo_name", git_url[1], "/tmp", file_paths, logger=mock_logger)

    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    for file_path in file_paths:
        app_file_contents = expected_apps[file_path]
        print(f"app_file_contents: {app_file_contents}")

        # Prepare the mock for file existence
        mock_exists, _ = mock_path

        # Simulate that the files exist
        mock_exists.return_value = True

        # Convert the dictionary into a YAML string (assuming app_files[1] is a dict)
        print(f"expected_apps:\n{app_file_contents}")
        yaml_data = yaml.dump(app_file_contents, default_flow_style=False)
        print(f"yaml_data:\n{yaml_data}")

        # Create mock for open() that simulates reading a YAML file
        mocked_open = mock_open(read_data=yaml_data)

        # Use patch to mock Path.open for the YAML files
        with patch.object(Path, "open", mocked_open):

            # Call the method under test
            handler.load_app_files()

            mocked_open.assert_has_calls([call("r")])

        assert file_path in handler.apps


def test_load_app_files_yaml_error(mock_subprocess, git_url, mock_logger, app_files, mock_path):
    """
    Tests error handling when invalid YAML is encountered.
    Verifies that the correct number of file opens occur and YAMLError is logged.
    Ensures the handler's `apps` remains empty due to the error.
    """
    file_paths, expected_apps = app_files

    # Initialize the handler with mock data
    handler = GitHandler("repo_name", git_url[1], "/tmp", file_paths, logger=mock_logger)

    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    for file_path in file_paths:
        app_file_contents = expected_apps[file_path]
        print(f"app_file_contents: {app_file_contents}")

        # Prepare the mock for file existence
        mock_exists, _ = mock_path

        # Simulate that the files exist
        mock_exists.return_value = True

        # Simulate a YAML parsing error by creating invalid YAML data
        invalid_yaml_data = "key: value: unquoted string"  # This will trigger a YAMLError

        # Create mock for open() that simulates reading a YAML file
        mocked_open = mock_open(read_data=invalid_yaml_data)

        # Use patch to mock Path.open for the YAML files
        with patch.object(Path, "open", mocked_open):

            # Call the method under test
            handler.load_app_files()

            mocked_open.assert_has_calls([call("r")])

        assert file_path not in handler.apps


def test_load_app_files_os_error(mock_subprocess, git_url, mock_logger, app_files, mock_path):
    """
    Tests error handling when an OSError (e.g., permission denied) occurs.
    Verifies the correct number of file opens and logs the OSError.
    Ensures the handler's `apps` remains empty due to the error.
    """
    file_paths, expected_apps = app_files

    # Initialize the handler with mock data
    handler = GitHandler("repo_name", git_url[1], "/tmp", file_paths, logger=mock_logger)

    mock_subprocess.assert_has_calls([call(["git", "clone", git_url[1], "/tmp/repo_name", "--recursive"], check=True)])

    for file_path in file_paths:
        app_file_contents = expected_apps[file_path]
        print(f"app_file_contents: {app_file_contents}")

        # Prepare the mock for file existence
        mock_exists, _ = mock_path

        # Simulate that the files exist
        mock_exists.return_value = True

        # Simulate an OSError when attempting to open a file
        mocked_open = mock_open()
        mocked_open.side_effect = OSError("Permission denied")

        # Use patch to mock Path.open for the YAML files
        with patch.object(Path, "open", mocked_open):

            # Call the method under test
            handler.load_app_files()

            mocked_open.assert_has_calls([call("r")])

        mock_logger.error.assert_any_call(f"Failed to read file /tmp/repo_name/{file_path}: Permission denied")

        assert file_path not in handler.apps
