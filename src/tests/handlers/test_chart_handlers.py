"""
ChartHandler test harness
"""

import subprocess
from unittest.mock import call, patch

import pytest
import yaml

from handlers.chart import ChartHandler


@pytest.fixture
def app_file():
    """
    Provides valid application configurations for tests.
    """
    return {
        "app1": {"installType": "helm", "chartName": "chart1", "repoURL": "http://repo1.com"},
        "app2": {"installType": "other", "chartName": "chart2", "repoURL": "http://repo2.com"},
    }


@pytest.fixture
def invalid_app_file():
    """
    Provides invalid application configurations for negative test cases.
    """
    return {
        "app1": {"chartName": "chart1"},  # Missing "installType"
        "app2": None,  # Invalid config
    }


@pytest.fixture
def mock_subprocess():
    """
    Fixture to mock subprocess.run for testing Helm commands.
    Mocks the subprocess.run function used for managing chart repositories.
    """
    with patch("subprocess.run") as mock_run:
        yield mock_run


@pytest.fixture
def mock_yaml_load():
    """
    Fixture to mock yaml.safe_load for testing configuration parsing.
    Mocks the yaml.safe_load function used parsing yaml string configurations.
    """
    with patch("yaml.safe_load") as mock_yaml_safe_load:
        yield mock_yaml_safe_load


@pytest.fixture
def chart_handler(app_file):
    """
    Initializes ChartHandler with a valid app configuration file.
    """
    return ChartHandler(_app_file=app_file)


def test_load_apps(chart_handler):
    """
    Validates that valid apps are loaded and invalid apps are excluded.
    """
    assert "app1" in chart_handler.charts
    assert "app2" not in chart_handler.charts


def test_load_apps_failure(mock_yaml_load, invalid_app_file, caplog):
    """
    Tests handling of invalid app configurations in load_apps.
    """
    mock_yaml_load.side_effect = ValueError("Invalid YAML")

    with patch("subprocess.run", side_effect=subprocess.CalledProcessError(1, "helm")):
        ChartHandler(_app_file=invalid_app_file)
        print(mock_yaml_load)
        assert "Failed to parse app1 configuration" in caplog.text
        assert "Failed to parse app2 configuration" in caplog.text


def test_add_chart_repo(mock_subprocess, chart_handler):
    """
    Tests adding a Helm chart repository successfully.
    """
    chart_handler.add_chart_repos()
    for chart, chart_config in chart_handler.charts.items():
        print(chart, chart_config)
        mock_subprocess.assert_called_with(
            ["helm", "repo", "add", chart_config["chartName"], chart_config["repoURL"]], check=True
        )


def test_add_chart_repo_failure(mock_subprocess, chart_handler, caplog):
    """
    Tests failure in adding a Helm chart repository.
    """
    mock_subprocess.side_effect = subprocess.CalledProcessError(1, "helm")

    for chart, chart_config in chart_handler.charts.items():
        print(chart, chart_config)
        chart_handler.add_chart_repos()
        mock_subprocess.assert_called_with(
            ["helm", "repo", "add", chart_config["chartName"], chart_config["repoURL"]], check=True
        )
        assert "Failed to execute helm command" in caplog.text


def test_update_existing_chart_repo_success(mock_subprocess, chart_handler):
    """
    Tests successfully updating an existing Helm chart repository with the correct URL.
    """
    with patch.object(ChartHandler, "check_helm_repo_details", return_value=(True, True)) as mock_check_repo_details:
        # Simulate successful repository update
        chart_handler.update_chart_repos()

        for chart, chart_config in chart_handler.charts.items():
            print(chart, chart_config)

            # Assert that the repository check was performed
            mock_check_repo_details.assert_called_once_with(
                _repo_name=chart_config["chartName"], _repo_url=chart_config["repoURL"]
            )

            # Assert that the helm command for updating the repository was executed
            mock_subprocess.assert_called_with(["helm", "repo", "update", chart_config["chartName"]], check=True)


def test_update_incorrect_repo_url(mock_subprocess, chart_handler, caplog):
    """
    Tests correcting an incorrect repository URL.
    """
    # Mocking the return value for subprocess.run when listing repos
    mock_subprocess.return_value.stdout = '[{"name": "chart1", "url": "http://wrong-url.com"}]'

    with patch.object(ChartHandler, "check_helm_repo_details", return_value=(True, False)) as mock_check_repo_details:
        # Call the method under test
        chart_handler.update_chart_repos()

        for chart, chart_config in chart_handler.charts.items():
            print(chart, chart_config)
            mock_check_repo_details.assert_any_call(
                _repo_name=chart_config["chartName"], _repo_url=chart_config["repoURL"]
            )

            # Print all calls to subprocess.run for debugging
            print("All subprocess calls:")
            for subprocess_call in mock_subprocess.call_args_list:
                print(subprocess_call)

            # Assertions
            assert f"Deleting repository {chart_config['chartName']} due to incorrect url" in caplog.text
            assert (
                f"Adding deleted repository {chart_config['chartName']} with correct url {chart_config["repoURL"]}"
                in caplog.text
            )

            mock_subprocess.assert_has_calls(
                [
                    call(["helm", "repo", "remove", chart_config["chartName"]], check=True),
                    call(["helm", "repo", "add", chart_config["chartName"], chart_config["repoURL"]], check=True),
                ]
            )


def test_update_missing_chart_repo(mock_subprocess, chart_handler, caplog):
    """
    Tests adding a missing Helm chart repository.
    """
    mock_subprocess.return_value.stdout = '[{"name": "chart2", "url": "http://repo2.com"}]'
    chart_handler.update_chart_repos()

    for chart, chart_config in chart_handler.charts.items():
        print(chart, chart_config)
        assert f"Chart repository {chart_config['chartName']} not found" in caplog.text

    mock_subprocess.assert_has_calls(
        [
            call(["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True),
            call(["helm", "repo", "add", "chart1", "http://repo1.com"], check=True),
        ]
    )


def test_delete_chart_repo(mock_subprocess, chart_handler):
    """
    Tests deleting a Helm chart repository successfully.
    """
    chart_handler.delete_chart_repos()
    mock_subprocess.assert_called_with(["helm", "repo", "remove", "chart1"], check=True)


def test_delete_chart_repo_failure(mock_subprocess, chart_handler, caplog):
    """
    Tests failure in deleting a Helm chart repository.
    """
    print(caplog)
    chart_handler.delete_chart_repos()
    mock_subprocess.assert_called_once_with(["helm", "repo", "remove", "chart1"], check=True)


def test_get_remote_apps(mock_yaml_load, mock_subprocess, chart_handler):
    """
    Tests fetching remote app versions successfully.
    """
    # Mock yaml.safe_load to return the expected parsed data
    mock_yaml_load.return_value = [{"version": "1.0.0"}]

    call_list = []
    # Simulate fetching remote apps for each chart
    for chart, chart_config in chart_handler.charts.items():
        print(f"Testing chart: {chart}, config: {chart_config}")

        # Call the method under test
        chart_handler.get_remote_apps(_chart_name=chart)

        # Log the state for debugging
        print("Remote charts:", chart_handler.remote_charts)

        # Assertions
        assert "app1" in chart_handler.remote_charts
        assert chart_handler.remote_charts[chart]["version"] == "1.0.0"

        call_list.append(
            call(
                ["helm", "search", "repo", f"{chart}/{chart}", "-o", "json"], capture_output=True, text=True, check=True
            )
        )

    # Assert subprocess calls for all charts
    mock_subprocess.assert_has_calls(call_list)


def test_get_remote_charts_failure(mock_yaml_load, mock_subprocess, chart_handler):
    """
    Tests failure in fetching remote charts due to errors.
    """
    # Mock subprocess to simulate `helm` command output
    mock_subprocess.return_value.stdout = ""

    # Mock yaml.safe_load to return the expected parsed data
    mock_yaml_load.side_effect = ValueError("Invalid YAML")

    # Call the method under test
    remote_chart_details = chart_handler.get_remote_charts(chart_name="app1")

    # Assertions
    assert remote_chart_details == {}


def test_get_local_chart_failure(chart_handler, caplog):
    """
    Tests failure in retrieving a nonexistent local chart version.
    """
    chart_handler.get_local_charts("nonexistent_chart")
    assert "Failed to load nonexistent_chart from local charts" in caplog.text


def test_get_charts_failure(mock_subprocess, chart_handler):
    """
    Tests failure in retrieving missing chart versions.
    """

    # Mock subprocess to simulate `helm` command output
    mock_subprocess.return_value.stdout = ""

    local_chart, remote_chart = chart_handler.get_chart_details("app1")

    mock_subprocess.assert_has_calls(
        [call(["helm", "search", "repo", "app1/app1", "-o", "json"], capture_output=True, text=True, check=True)]
    )
    assert local_chart == chart_handler.charts["app1"]
    assert remote_chart == {}


def test_check_helm_repo_details_repo_found_url_correct(mock_subprocess, chart_handler):
    """
    Tests when the repository is found and the URL is correct.
    """
    # Mock subprocess output to simulate the correct repo details
    mock_subprocess.return_value.stdout = yaml.dump([{"name": "chart1", "url": "http://repo1.com"}])

    repo_found, url_correct = chart_handler.check_helm_repo_details(_repo_name="chart1", _repo_url="http://repo1.com")
    print(chart_handler.charts)
    # Assertions
    assert repo_found is True
    assert url_correct is True
    mock_subprocess.assert_called_once_with(
        ["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True
    )


def test_check_helm_repo_details_repo_found_url_incorrect(mock_subprocess, chart_handler, caplog):
    """
    Tests when the repository is found but the URL is incorrect.
    """
    # Mock subprocess output to simulate the incorrect repo URL
    mock_subprocess.return_value.stdout = yaml.dump([{"name": "chart1", "url": "http://wrong-url.com"}])

    repo_found, url_correct = chart_handler.check_helm_repo_details(_repo_name="chart1", _repo_url="http://repo1.com")

    # Assertions
    assert repo_found is True
    assert url_correct is False
    mock_subprocess.assert_called_once_with(
        ["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True
    )
    assert "Chart repository url incorrect" in caplog.text


def test_check_helm_repo_details_repo_not_found(mock_subprocess, chart_handler, caplog):
    """
    Tests when the repository is not found.
    """
    # Mock subprocess output to simulate missing repo
    mock_subprocess.return_value.stdout = yaml.dump([{"name": "chart2", "url": "http://repo2.com"}])

    repo_found, url_correct = chart_handler.check_helm_repo_details(_repo_name="chart1", _repo_url="http://repo1.com")

    # Assertions
    assert repo_found is False
    assert url_correct is False
    mock_subprocess.assert_called_once_with(
        ["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True
    )
    assert "Chart repository chart1 not found" in caplog.text


def test_check_helm_repo_details_command_failure(mock_subprocess, chart_handler, caplog):
    """
    Tests when the subprocess call fails.
    """
    # Simulate subprocess error
    mock_subprocess.side_effect = subprocess.CalledProcessError(1, "helm")

    repo_found, url_correct = chart_handler.check_helm_repo_details(_repo_name="chart1", _repo_url="http://repo1.com")

    # Assertions
    assert repo_found is False
    assert url_correct is False
    mock_subprocess.assert_called_once_with(
        ["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True
    )
    assert "Failed to interrogate repository with command" in caplog.text


def test_check_helm_repo_details_invalid_json(mock_subprocess, chart_handler, caplog):
    """
    Tests when the subprocess returns invalid JSON.
    """
    # Mock subprocess output with invalid JSON
    mock_subprocess.return_value.stdout = "INVALID_JSON"

    repo_found, url_correct = chart_handler.check_helm_repo_details(_repo_name="chart1", _repo_url="http://repo1.com")

    # Assertions
    assert repo_found is False
    assert url_correct is False
    mock_subprocess.assert_called_once_with(
        ["helm", "repo", "list", "-o", "json"], capture_output=True, text=True, check=True
    )
    assert "Failed to interrogate repository with command" in caplog.text


@pytest.mark.parametrize(
    "mock_configuration, log_error",
    [
        # Case: Versions are the same
        (
            ("1.0.0", "1.0.0", False),
            None,
        ),
        # Case: Versions are different (outdated)
        (
            ("1.0.0", "2.0.0", True),
            None,
        ),
        # Case: Remote chart data is missing
        (
            ("1.0.0", "", None),
            "Failed to fetch versions",
        ),
        # Case: Local chart data is missing
        (
            ("", "1.0.0", None),
            "Failed to fetch versions",
        ),
        # Case: Both charts missing
        (
            ("", "", None),
            "Failed to fetch versions",
        ),
    ],
)
def test_compare_version(mock_configuration, log_error, chart_handler, mock_subprocess, mock_yaml_load):
    """
    Tests different scenarios concerning chart version comparisons
    """

    local_version, remote_version, outdated_flag = mock_configuration
    print(f"local_version: {local_version}")
    print(f"remote_version: {remote_version}")
    print(f"outdated_flag: {outdated_flag}")

    # Mock chart under test
    chart = "chart1"

    assert chart == chart_handler.charts["app1"]["chartName"]

    chart_handler.charts["app1"]["targetRevision"] = local_version
    print(chart_handler.charts["app1"])

    # Mock yaml output to simulate the expected return values
    if outdated_flag is None:
        mock_subprocess.side_effect = subprocess.CalledProcessError(1, log_error)
    else:
        # Mock subprocess output to simulate the correct repo details
        mock_subprocess.return_value.stdout = yaml.dump([{"name": chart, "url": "http://repo1.com"}])
        mock_yaml_load.return_value = [{"version": remote_version}]

    # Method under test
    actual_local_version, actual_remote_version, outdated = chart_handler.compare_version(chart_name="app1")
    print(f"actual_local_version: {actual_local_version}")
    print(f"actual_remote_version: {actual_remote_version}")
    print(f"outdated: {outdated}")

    # Assertions
    if outdated_flag is not None:
        assert local_version == actual_local_version
        assert remote_version == actual_remote_version
        assert outdated_flag == outdated
    else:
        assert actual_local_version == ""
        assert actual_remote_version == ""
        assert outdated_flag is None
