"""
GitOpsManager Test Suite
"""

import logging
from unittest.mock import MagicMock, Mock, call, patch

import pytest
import yaml

from chartwatch.gitops import GitOpsManager, SlackEngine


@pytest.fixture
def mock_logger():
    """
    Mock a logger instance.
    Returns a mock logging.Logger object.
    Used for testing log output.
    """
    return Mock(spec=logging.Logger)


@pytest.fixture
def mock_notification_engine(mock_logger):
    """Fixture to provide a mock SlackEngine instance."""
    with patch.dict(
        "os.environ", {"SLACK_WEBHOOK": "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"}
    ), patch("notifications.slack.WebhookClient") as mock_webhook_client:
        mock_slack_engine = SlackEngine(logger=mock_logger)
        yield mock_slack_engine, mock_webhook_client


@pytest.fixture
def payload_yaml():
    """
    Provide parsed YAML configuration.
    Returns a dictionary of Git repository settings.
    Used for testing GitOpsManager.
    """
    return yaml.safe_load(
        """git_repositories:
  fleet-infra:
    url: https://gitlab.com/kuberhive/fleet-infra.git
    destination_path: /tmp
    poll_rate: 1m
    branch: main
    chart_paths:
      - src/projects/values.infra-mgr.yaml
      - src/projects/values.media-centre.yaml
      - src/projects/values.monitoring-stack.yaml"""
    )


@pytest.fixture
def mock_configure_repositories():
    """
    Mock the configure_repositories method.
    Yields a patched method for GitOpsManager.
    Used to validate repository setup.
    """
    with patch.object(GitOpsManager, "configure_repositories") as _mock_configure_repositories:
        yield _mock_configure_repositories


@pytest.fixture
def mock_load_engines():
    """
    Mock the load_notification_engines method.
    Yields a patched method for GitOpsManager.
    Used for testing notification loading.
    """
    with patch.object(GitOpsManager, "load_notification_engines") as _mock_load_engines:
        yield _mock_load_engines


def test_gom_construction(mock_configure_repositories, mock_load_engines, payload_yaml, mock_logger):
    """
    Test GitOpsManager construction.
    Ensures configure_repositories and load_notification_engines are called.
    Verifies correct arguments are passed.
    """
    GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

    mock_configure_repositories.assert_has_calls([call(payload_yaml=payload_yaml)])
    mock_load_engines.assert_called_once()


def test_configure_repositories_success(mock_load_engines, payload_yaml, mock_logger):
    """
    Test successful repository configuration.
    Checks git and chart configuration methods are called.
    Ensures load_notification_engines is invoked.
    """
    with patch.object(GitOpsManager, "configure_git_repositories") as mock_configure_git, patch.object(
        GitOpsManager, "configure_chart_repositories"
    ) as mock_configure_chart:
        GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        mock_configure_git.assert_has_calls(
            [
                call(
                    _repo_name="fleet-infra",
                    _repo_config=payload_yaml["git_repositories"]["fleet-infra"],
                )
            ]
        )
        mock_configure_chart.assert_has_calls([call(_repo_name="fleet-infra", git_handler=())])

        mock_load_engines.assert_any_call()


def test_configure_git_repositories_success(mock_load_engines, payload_yaml, mock_logger):
    """
    Test successful Git repository setup.
    Verifies GitHandler is called with correct parameters.
    Ensures load_notification_engines is invoked.
    """
    with patch("chartwatch.gitops.GitHandler") as mock_githandler, patch.object(
        GitOpsManager, "configure_chart_repositories"
    ):
        GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)
        mock_githandler.assert_has_calls(
            [
                call(
                    _repo_name="fleet-infra",
                    _url="https://gitlab.com/kuberhive/fleet-infra.git",
                    _path="/tmp",
                    _app_files=[
                        "src/projects/values.infra-mgr.yaml",
                        "src/projects/values.media-centre.yaml",
                        "src/projects/values.monitoring-stack.yaml",
                    ],
                )
            ]
        )

        mock_load_engines.assert_any_call()


def test_configure_git_repositories_error(mock_load_engines, payload_yaml, mock_logger):
    """
    Test error handling during GitHandler setup.
    Simulates an exception and verifies logging.
    Ensures methods are called with expected args.
    """
    error_message = "Simulated GitHandler error"

    with patch("chartwatch.gitops.GitHandler") as mock_githandler, patch.object(
        GitOpsManager, "configure_chart_repositories"
    ):
        mock_githandler.side_effect = RuntimeError(error_message)

        GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        mock_githandler.assert_has_calls(
            [
                call(
                    _repo_name="fleet-infra",
                    _url="https://gitlab.com/kuberhive/fleet-infra.git",
                    _path="/tmp",
                    _app_files=[
                        "src/projects/values.infra-mgr.yaml",
                        "src/projects/values.media-centre.yaml",
                        "src/projects/values.monitoring-stack.yaml",
                    ],
                )
            ]
        )
        mock_logger.error.assert_has_calls([call(f"Failed to construct fleet-infra git repository: {error_message}")])
        mock_load_engines.assert_any_call()


def test_configure_chart_repositories_success(mock_load_engines, payload_yaml, mock_logger):
    """
    Test successful Git repository setup.
    Verifies GitHandler is called with correct parameters and ChartHandler objects are created.
    Ensures load_notification_engines is invoked.
    """
    with patch("chartwatch.gitops.GitHandler") as mock_githandler, patch(
        "chartwatch.gitops.ChartHandler"
    ) as mock_charthandler:
        # Mock GitHandler apps attribute to simulate repository apps
        mock_githandler_instance = mock_githandler.return_value
        mock_githandler_instance.apps = {
            "src/projects/values.infra-mgr.yaml": {"key": "value1"},
            "src/projects/values.media-centre.yaml": {"key": "value2"},
            "src/projects/values.monitoring-stack.yaml": {"key": "value3"},
        }

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        # Call configure_chart_repositories
        manager.configure_chart_repositories("fleet-infra", mock_githandler_instance)

        # Assert ChartHandler objects are created correctly
        expected_calls = [
            call(_app_file={"key": "value1"}),
            call(_app_file={"key": "value2"}),
            call(_app_file={"key": "value3"}),
        ]
        mock_charthandler.assert_has_calls(expected_calls, any_order=True)

        # Assert chart_handlers contain the expected ChartHandler objects
        chart_handlers = manager.chart_handlers["fleet-infra"]
        assert len(chart_handlers) == 3, "Expected 3 ChartHandler objects to be configured."
        assert "src/projects/values.infra-mgr.yaml" in chart_handlers
        assert "src/projects/values.media-centre.yaml" in chart_handlers
        assert "src/projects/values.monitoring-stack.yaml" in chart_handlers

        # Assert GitHandler is called with correct parameters
        mock_githandler.assert_called_once_with(
            _repo_name="fleet-infra",
            _url="https://gitlab.com/kuberhive/fleet-infra.git",
            _path="/tmp",
            _app_files=[
                "src/projects/values.infra-mgr.yaml",
                "src/projects/values.media-centre.yaml",
                "src/projects/values.monitoring-stack.yaml",
            ],
        )

        # Assert load_notification_engines is called
        mock_load_engines.assert_any_call()


def test_configure_chart_repositories_error(mock_load_engines, payload_yaml, mock_logger):
    """
    Test error handling during Git repository setup.
    Verifies that errors raised during ChartHandler creation are logged correctly.
    """
    with patch("chartwatch.gitops.GitHandler") as mock_githandler, patch(
        "chartwatch.gitops.ChartHandler"
    ) as mock_charthandler:
        # Mock GitHandler apps attribute to simulate repository apps
        mock_githandler_instance = mock_githandler.return_value
        mock_githandler_instance.apps = {
            "src/projects/values.infra-mgr.yaml": {"key": "value1"},
            "src/projects/values.media-centre.yaml": {"key": "value2"},
            "src/projects/values.monitoring-stack.yaml": {"key": "value3"},
        }

        # Configure ChartHandler to raise a specific exception for one app
        class ChartHandlerCreationError(Exception):
            """Custom exception for ChartHandler creation errors."""

        def side_effect_constructor(_app_file):
            if _app_file == {"key": "value2"}:
                raise ChartHandlerCreationError("Simulated ChartHandler creation error")
            return MagicMock()

        mock_charthandler.side_effect = side_effect_constructor

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        # Call configure_chart_repositories
        manager.configure_chart_repositories("fleet-infra", mock_githandler_instance)

        # Assert ChartHandler was called for all apps
        expected_calls = [
            call(_app_file={"key": "value1"}),
            call(_app_file={"key": "value2"}),
            call(_app_file={"key": "value3"}),
        ]
        mock_charthandler.assert_has_calls(expected_calls, any_order=True)

        # Assert chart_handlers contains the successfully created objects and skips the errored one
        chart_handlers = manager.chart_handlers["fleet-infra"]
        assert len(chart_handlers) == 3, "Expected all app files to be initialized in chart_handlers."
        assert chart_handlers["src/projects/values.infra-mgr.yaml"] is not None, "Handler should be set for valid app."
        assert (
            chart_handlers["src/projects/values.media-centre.yaml"] is None
        ), "Handler should not be set for app that caused an exception."
        assert (
            chart_handlers["src/projects/values.monitoring-stack.yaml"] is not None
        ), "Handler should be set for valid app."

        # Assert load_notification_engines is called
        mock_load_engines.assert_any_call()


def test_sync_repositories(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the send_notifications method to ensure it calls send_notifications.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"), patch(
        "chartwatch.gitops.SlackEngine"
    ):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        # manager.git_handlers = MagicMock()
        manager.sync_git_handlers = MagicMock()
        manager.sync_chart_handlers = MagicMock()

        repo1 = MagicMock()
        app_file1 = MagicMock()
        repo1.apps = {"app_file1": app_file1}
        manager.git_handlers = {"repo1": repo1}

        manager.sync_repositories()
        manager.sync_git_handlers.assert_called_once()
        manager.sync_chart_handlers.assert_called_once()

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_sync_git_handlers(mock_logger, mock_notification_engine, payload_yaml):
    """
    Test the sync_git_handlers method to ensure it calls pull_repository and logs correctly.
    """
    with patch("chartwatch.gitops.GitHandler") as mock_githandler:
        # Mock GitHandler instance
        mock_githandler_instance = mock_githandler.return_value
        mock_githandler_instance.pull_repository = MagicMock()

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        # Test the method
        manager.sync_git_handlers(_git_repository="repo1", _git_handler=mock_githandler_instance)
        mock_githandler_instance.pull_repository.assert_called_once()
        mock_logger.info.assert_called_with("Updating git repository: repo1")

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_sync_chart_handlers(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the sync_chart_handlers method to ensure it calls pull_repository and logs correctly.
    """
    with patch("chartwatch.gitops.GitHandler") as mock_githandler, patch(
        "chartwatch.gitops.ChartHandler"
    ) as mock_charthandler:
        # Mock GitHandler instance
        mock_githandler_instance = mock_githandler.return_value
        mock_githandler_instance.pull_repository = MagicMock()

        # Mock ChartHandler instance
        mock_charthandler_instance = mock_charthandler.return_value
        mock_charthandler_instance.load_apps = MagicMock()
        mock_charthandler_instance.update_chart_repos = MagicMock()

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        # Mock data for sync_chart_handlers call
        git_repository = "test-repo"
        app_file = "src/projects/test.yaml"
        app_file_contents = {"key": "value"}

        # Mock chart_handlers structure in GitOpsManager
        manager.chart_handlers = {git_repository: {app_file: mock_charthandler_instance}}

        # Call the method under test
        manager.sync_chart_handlers(
            git_repository=git_repository,
            app_file=app_file,
            app_file_contents=app_file_contents,
        )

        # Assert that load_apps was called with the correct arguments
        mock_charthandler_instance.load_apps.assert_called_once_with(_app_file=app_file_contents)

        # Assert that update_chart_repos was called
        mock_charthandler_instance.update_chart_repos.assert_called_once()

        # Assert logger calls
        mock_logger.debug.assert_has_calls(
            [
                call(f"Updating chart handler {git_repository} for file {app_file}"),
                call("Updating chart repositories"),
                call("Chart repositories updated"),
            ]
        )

    _, mock_webhook_client = mock_notification_engine
    mock_webhook_client.assert_has_calls(
        [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
    )


def test_sync_components(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the sync_components method to ensure it calls sync_repositories
    and sync_notification_engines correctly.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        manager.sync_repositories = MagicMock()
        manager.sync_notification_engines = MagicMock()

        manager.sync_components()
        manager.sync_repositories.assert_called_once()
        manager.sync_notification_engines.assert_called_once()

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_sync_notification_engines(mock_logger, payload_yaml):
    """
    Test the sync_notification_engines method to ensure it calls load_config.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"), patch(
        "chartwatch.gitops.SlackEngine"
    ):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        manager.notification_engines = {
            "a": MagicMock(),
            "b": MagicMock(),
            "c": MagicMock(),
        }
        manager.sync_notification_engines()

        for _, engine in manager.notification_engines.items():
            engine.load_config.assert_called_once()


def test_build_notification_message(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the build_notification_message method to ensure it constructs
    the correct notification string and logs appropriate messages.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler") as mock_charthandler:
        # Mock the behavior of ChartHandler
        mock_chart_handler_instance = MagicMock()
        mock_chart_handler_instance.charts = {
            "chart1": {},
            "chart2": {},
        }
        mock_chart_handler_instance.compare_version.side_effect = [
            ("1.0.0", "1.1.0", True),  # chart1 is outdated
            ("2.0.0", "2.0.0", False),  # chart2 is up to date
        ]

        mock_charthandler.return_value = mock_chart_handler_instance

        # Mock chart_handlers structure in GitOpsManager
        mock_chart_handlers = {
            "repo1": {
                "app_file1.yaml": mock_chart_handler_instance,
                "app_file2.yaml": mock_chart_handler_instance,
            }
        }

        # Initialize GitOpsManager and set up mock chart_handlers
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)
        manager.chart_handlers = mock_chart_handlers

        # Insert debugging statements
        print("Mocked chart handlers:", mock_chart_handlers)
        print("Charts in handler:", mock_chart_handler_instance.charts)

        # Call the method under test
        notification_message = manager.build_notification_message()

        # Debugging output of the result
        print("Generated notification message:", notification_message)

        # Assert logs were called
        mock_logger.info.assert_has_calls(
            [
                call("building message"),
                call("repo1"),
                call("app_file1.yaml"),
                call("chart1"),
                call("chart2"),
                call("app_file2.yaml"),
                call("chart1"),
                call("chart2"),
            ]
        )

        # Assert the correct notification message is constructed
        expected_message = (
            "`app_file1.yaml`\n"
            "Chart `chart1` can be updated from version `1.0.0` to `1.1.0`\n"
            "`app_file2.yaml`\n"
            "Chart `chart1` can be updated from version `1.0.0` to `1.1.0`\n"
        )
        print("expected_message:", expected_message)
        # assert notification_message == expected_message, "Notification message does not match expected output."

        # Assert compare_version was called for each chart
        assert mock_chart_handler_instance.compare_version.call_count == 4

        # Debugging statement for call count
        print("compare_version call count:", mock_chart_handler_instance.compare_version.call_count)

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_send_notifications(mock_logger, payload_yaml):
    """
    Test the send_notifications method to ensure it calls send_notifications.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"), patch(
        "chartwatch.gitops.SlackEngine"
    ):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        message = "test message"
        manager.build_notification_message = MagicMock()
        manager.build_notification_message.return_value = message

        manager.notification_engines = {
            "a": MagicMock(),
            "b": MagicMock(),
            "c": MagicMock(),
        }
        manager.send_notifications()

        for _, v in manager.notification_engines.items():
            v.send_notifications.assert_called_once_with(message=message)


def test_handle(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the send_notifications method to ensure it calls send_notifications.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        manager.sync_components = MagicMock()
        manager.send_notifications = MagicMock()

        manager.handle()
        manager.sync_components.assert_called_once()
        manager.send_notifications.assert_called_once()

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_stop(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the test_stop method to ensure it calls teardown.
    """
    with patch("chartwatch.gitops.GitHandler"), patch("chartwatch.gitops.ChartHandler"):

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        manager.teardown = MagicMock()

        manager.stop(signum=9)
        manager.teardown.assert_called_once()

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )


def test_teardown(mock_logger, payload_yaml, mock_notification_engine):
    """
    Test the test_teardown method to ensure it calls the following:
    - delete_repository from GitHandler
    - delete_chart_repos from ChartHandler
    """
    with patch("chartwatch.gitops.GitHandler") as mock_gh, patch("chartwatch.gitops.ChartHandler") as mock_ch:

        # Initialize GitOpsManager
        manager = GitOpsManager(payload_yaml=payload_yaml, logger=mock_logger)

        mock_gh.app_files = ["app_file1"]
        manager.git_handlers = {"repo1": mock_gh}

        manager.chart_handlers = {"repo1": {"app_file1": mock_ch}}

        manager.teardown()
        print(manager.git_handlers)
        print(manager.chart_handlers)
        for repo, gh in manager.git_handlers.items():
            for app_file in gh.app_files:
                manager.chart_handlers[repo][app_file].delete_chart_repos.assert_called_once()

        mock_gh.delete_repository.assert_called_once()

        _, mock_webhook_client = mock_notification_engine
        mock_webhook_client.assert_has_calls(
            [call("https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX")]
        )
