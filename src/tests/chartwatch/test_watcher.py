"""
Watcher Test Suite
Unit tests for the Watcher class, including testing YAML loading,
error handling, and lifecycle management methods.
"""

import logging
from threading import Event, Thread
from unittest.mock import MagicMock, Mock, call, mock_open, patch

import pytest
import yaml

from chartwatch.watcher import Watcher


@pytest.fixture
def mock_os():
    """
    Fixture for mocking the OS environment variables.
    Mocks SLACK_WEBHOOK for testing purposes.
    """
    with patch.dict(
        "os.environ", {"SLACK_WEBHOOK": "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"}
    ) as mock_os:
        yield mock_os


@pytest.fixture
def mock_payload() -> dict:
    """
    Fixture providing a mock payload YAML file path and content.
    Includes mock repository configurations for tests.
    """
    return {
        "path": "/tmp/foo/payload.yaml",
        "content": """git_repositories:
  fleet-infra:
    url: https://gitlab.com/kuberhive/fleet-infra.git
    destination_path: /tmp
    poll_rate: 1m
    branch: main
    chart_paths:
      - src/projects/values.infra-mgr.yaml
      - src/projects/values.media-centre.yaml
      - src/projects/values.monitoring-stack.yaml
        """,
    }


@pytest.fixture
def mock_logger():
    """
    Fixture for mocking a logger.
    Returns a mock logging.Logger object.
    """
    return Mock(spec=logging.Logger)


@pytest.fixture(
    params=[
        {"side_effect": FileNotFoundError, "error_type": FileNotFoundError},
        {"side_effect": PermissionError, "error_type": PermissionError},
        {"side_effect": OSError, "error_type": OSError},
    ]
)
def file_open_error_fixture(request):
    """
    Fixture to parameterize file open error scenarios.
    Provides side effects and expected error types.
    """
    return request.param


@pytest.fixture
def mock_load_yaml():
    """
    Fixture for mocking the load_yaml method.
    Provides mock YAML data for tests.
    """
    mock_payload = {"git_repositories": {"apps": {"app1": {"key": "value"}}}}
    with patch.object(Watcher, "load_yaml", return_value=mock_payload) as mock_yaml:
        yield mock_yaml


@pytest.fixture
def mock_gitops_manager():
    """
    Fixture for mocking GitOpsManager.
    Mocks its key methods for isolated testing.
    """
    with patch("chartwatch.watcher.GitOpsManager") as mock_gmanager:
        mock_gmanager.configure_repositories = MagicMock()
        mock_gmanager.configure_git_repositories = MagicMock()
        mock_gmanager.configure_chart_repositories = MagicMock()
        mock_gmanager.handle = MagicMock()
        mock_gmanager.stop = MagicMock()
        yield mock_gmanager


@pytest.fixture
def watcher_instance(mock_gitops_manager, mock_load_yaml):
    """
    Fixture for creating a Watcher instance.
    Uses mocked dependencies for isolated testing.
    """
    logger = logging.getLogger("test_logger")
    print(mock_gitops_manager)
    print(mock_load_yaml)
    return Watcher(polling_rate=0.1, payload_yaml_path="mock/path", logger=logger)


def test_chartwatch_construction_error(file_open_error_fixture):
    """
    Test construction of Watcher with file-related errors.
    Ensures appropriate exceptions are raised.
    """
    error_scenario = file_open_error_fixture

    with patch("builtins.open", mock_open()) as mocked_open:
        mocked_open.side_effect = error_scenario["side_effect"]
        with pytest.raises(error_scenario["error_type"]):
            Watcher(payload_yaml_path="")


def test_watcher_start_success(mock_gitops_manager, mock_load_yaml, watcher_instance):
    """
    Test the start method of Watcher for a successful polling loop.
    """
    gmanager_mock = mock_gitops_manager.return_value
    gmanager_mock.handle = MagicMock(side_effect=[None, None])  # Simulate two successful calls

    with patch.object(watcher_instance.logger, "info"):
        start_completed_event = Event()

        def start_watcher():
            watcher_instance.start()
            start_completed_event.set()

        watcher_thread = Thread(target=start_watcher, daemon=True)
        watcher_thread.start()

        # Allow thread to initialize
        start_completed_event.wait(0.1)

        # Validate that watcher starts
        assert watcher_instance.watch is True, "Watcher should be running after start is called."

        # Stop the watcher
        watcher_instance.stop()

        # Ensure shutdown_event is set and thread completes
        watcher_thread.join(timeout=2)
        assert not watcher_instance.watch, "Watcher should be stopped after calling stop."
        assert watcher_instance.shutdown_event.is_set(), "shutdown_event should be set after calling stop."
        mock_load_yaml.assert_has_calls([call(payload_yaml_path="mock/path")])


def test_watcher_start_failure(mock_gitops_manager, mock_load_yaml, watcher_instance):
    """
    Test the start method of Watcher handling a failure in the polling loop.
    """
    gmanager_mock = mock_gitops_manager.return_value
    gmanager_mock.handle = MagicMock(side_effect=[None, Exception("Test exception")])

    with patch.object(watcher_instance.logger, "info") as mock_info, patch.object(
        watcher_instance.logger, "error"
    ) as mock_error:
        start_completed_event = Event()

        def start_watcher():
            watcher_instance.start()
            start_completed_event.set()

        watcher_thread = Thread(target=start_watcher, daemon=True)
        watcher_thread.start()

        # Allow thread to initialize
        start_completed_event.wait(0.5)

        # Validate that watcher starts
        assert watcher_instance.watch is False, "Watcher should not be running after start is called."

        # Ensure shutdown_event is set and thread completes
        watcher_thread.join(timeout=2)
        assert not watcher_instance.watch, "Watcher should be stopped after calling stop."
        assert watcher_instance.shutdown_event.is_set(), "shutdown_event should be set after calling stop."

        # Validate logging calls
        mock_info.assert_any_call("starting the watcher")
        mock_error.assert_called_with("Error during handling: Test exception")
        assert gmanager_mock.handle.call_count == 2, "handle should be called twice (normal and exception)."
        assert mock_load_yaml.called, "load_yaml should be called during initialization."


def test_watcher_stop(mock_gitops_manager, watcher_instance):
    """
    Test the stop method of Watcher.
    Validates proper shutdown and cleanup behavior.
    """
    gmanager_mock = mock_gitops_manager.return_value
    gmanager_mock.stop = MagicMock()

    with patch.object(watcher_instance.logger, "warning") as mock_warning, patch.object(
        watcher_instance.logger, "info"
    ) as mock_info, patch.object(watcher_instance.logger, "debug") as mock_debug:
        watcher_instance.stop(signum=2, frame="mock_frame")

        mock_warning.assert_called_with("Stopping the watcher due to signal 2")
        mock_debug.assert_called_with("frame: mock_frame")
        gmanager_mock.stop.assert_called_once_with(signum=2)
        mock_info.assert_called_with("Watcher has stopped.")
        assert not watcher_instance.watch, "Watcher.watch should be set to False."
        assert watcher_instance.shutdown_event.is_set(), "Watcher.shutdown_event should be set."


def test_load_yaml_success(mock_payload, mock_os, mock_logger):
    """
    Test that load_yaml successfully loads and parses a valid YAML file.
    Validates file open and YAML parsing functionality.
    """
    print(mock_os)
    mock_path = mock_payload["path"]
    mock_content = mock_payload["content"]

    with patch("builtins.open", mock_open(read_data=mock_content)) as mocked_file, patch.object(
        yaml, "safe_load", wraps=yaml.safe_load
    ) as mocked_yaml, patch("subprocess.run") as mock_subprocess:
        watcher = Watcher(polling_rate=0.1, payload_yaml_path=mock_path, logger=mock_logger)
        yaml_data = watcher.load_yaml(payload_yaml_path=mock_path)

        print(mocked_yaml)
        print(mock_subprocess)
        mocked_file.assert_any_call(mock_path, "r", encoding="utf-8")
        assert yaml_data == yaml.safe_load(mock_content)


@pytest.mark.parametrize(
    "error_fixture",
    [
        {"side_effect": FileNotFoundError, "error_message": "No such file or directory"},
        {"side_effect": PermissionError, "error_message": "Permission denied"},
        {"side_effect": OSError, "error_message": "OS error"},
    ],
)
def test_load_yaml_file_errors(mock_payload, mock_logger, error_fixture):
    """
    Test that load_yaml raises appropriate exceptions for file-related errors.
    Covers FileNotFoundError, PermissionError, and OSError cases.
    """
    mock_path = mock_payload["path"]
    error_side_effect = error_fixture["side_effect"]

    with patch("builtins.open", side_effect=error_side_effect) as mocked_file, pytest.raises(error_side_effect):
        Watcher(polling_rate=0.1, payload_yaml_path=mock_path, logger=mock_logger)

    mocked_file.assert_called_once_with(mock_path, "r", encoding="utf-8")
