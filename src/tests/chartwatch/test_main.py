"""
Chartwatch Test suite
"""

import argparse
from unittest.mock import MagicMock, patch

import pytest

import chartwatch
import chartwatch.__main__

# Import the functions and classes from the __main__.py module
from chartwatch.__main__ import convert_to_seconds, parse_args, validate_polling_rate


def minutes_to_seconds(minutes):
    """Convert minutes to seconds."""
    return float(minutes) * 60


def hours_to_seconds(hours):
    """Convert hours to seconds."""
    return float(hours) * 3600


# Mock the Watcher class to prevent actual execution
mock_watcher_class = MagicMock()


@pytest.fixture(
    params=[
        "1",
        "2",
        "3",
    ]
)
def polling_rates_numbers(request):
    """Mocked input data for numerical value of pollng rates"""
    return request.param


@pytest.fixture(
    params=[
        "s",
        "m",
        "h",
    ]
)
def polling_rates_units(request):
    """Mocked input data for units of pollng rates"""
    return request.param


@pytest.fixture
def polling_rates(polling_rates_numbers, polling_rates_units):
    """Mocked pollng rates"""
    return f"{polling_rates_numbers}{polling_rates_units}"


def test_validate_polling_rate_valid(polling_rates):
    """Test valid polling rates"""
    assert validate_polling_rate(polling_rates) == polling_rates


def test_validate_polling_rate_invalid():
    """Test invalid polling rates"""
    with pytest.raises(argparse.ArgumentTypeError):
        validate_polling_rate("10x")
    with pytest.raises(argparse.ArgumentTypeError):
        validate_polling_rate("10")
    with pytest.raises(argparse.ArgumentTypeError):
        validate_polling_rate("h")


def test_convert_to_seconds(polling_rates, polling_rates_numbers, polling_rates_units):
    """Test conversion to seconds"""
    converted_polling_rate = convert_to_seconds(polling_rates)
    if polling_rates_units == "s":
        assert converted_polling_rate == float(polling_rates_numbers)
    elif polling_rates_units == "m":
        assert converted_polling_rate == minutes_to_seconds(polling_rates_numbers)
    elif polling_rates_units == "h":
        assert converted_polling_rate == hours_to_seconds(polling_rates_numbers)


@patch("chartwatch.__main__.argparse.ArgumentParser.parse_known_args")
def test_parse_args(mock_parse_known_args):
    """Test chartwatch argparse instructions"""
    mock_args = MagicMock()
    mock_args.config = "config.yaml"
    mock_args.polling_rate = "1s"
    mock_args.verbose = 1
    mock_parse_known_args.return_value = (mock_args, [])

    args, _ = parse_args()

    # Validate arguments
    assert args.config == "config.yaml"
    assert args.polling_rate == "1s"
    assert args.verbose == 1


@pytest.fixture(params=[1, 2, 3])
def verbosity(request):
    """Parameterized verbosity fixture."""
    return request.param


def test_main(verbosity):
    """Test main method of chartwatch application with different verbosity levels."""
    mock_args = MagicMock()
    mock_args.config = "config.yaml"
    mock_args.polling_rate = "1s"
    mock_args.verbose = verbosity

    with patch("chartwatch.__main__.Watcher", mock_watcher_class) as mock_watcher_class_instance, patch(
        "chartwatch.__main__.argparse.ArgumentParser.parse_known_args"
    ) as mock_parse_known_args, patch("chartwatch.__main__.signal.signal") as mock_signal, patch(
        "chartwatch.__main__.sys.exit"
    ) as mock_sys_exit:

        # Set up the mock return value
        mock_parse_known_args.return_value = (mock_args, [])

        # Mock logger to avoid actual logging
        with patch("chartwatch.__main__.logger") as mock_logger:
            # Run the main script logic
            with patch("chartwatch.__main__.__name__", "__main__"):
                with patch("chartwatch.__main__.sys.argv", ["__main__.py"]):
                    # Assertions
                    chartwatch.__main__.main()
                    assert mock_logger.info.call_count == 1
                    assert mock_watcher_class_instance.call_args[1]["payload_yaml_path"] == "config.yaml"
                    assert mock_signal.call_count == 3
                    mock_sys_exit.assert_called_once()


def test_main_entrypoint():
    """Test that main() is called when the script is executed as '__main__'."""
    with patch("chartwatch.__main__.main") as mock_main:
        # Simulate the script being run as the main module
        chartwatch.__main__.main()

        # Check if the main function is called once
        mock_main.assert_called_once()
