"""
NotificationsEngine Test Suite
"""

import logging
from unittest import mock

import pytest

from notifications.engine import NotificationsEngine


# Concrete subclass for testing
class MockNotificationsEngine(NotificationsEngine):
    """Concrete mock class for testing"""

    def __init__(self, logger: logging.Logger | None = None) -> None:
        """mock __init__ for testing"""
        self.logger = logger or logging.getLogger(__name__)

    def load_config(self) -> None:
        """mock load_config for testing"""

    def validate_config(self) -> None:
        """mock validate_config for testing"""

    def send_notifications(self, message: str) -> None:
        """mock send_notifications for testing"""


@pytest.fixture
def logger():
    """Mock logger"""
    return mock.Mock()


@pytest.fixture
def mock_notifications_engine(logger):
    """Fixture for creating a mock MockNotificationsEngine instance."""
    return MockNotificationsEngine(logger=logger)


def test_not_implemented_errors():
    """
    Test that calling the validate_payload method of a subclass of NotificationsEngine
    without overriding it raises NotImplementedError.
    """

    class TestNotificationsEngine(NotificationsEngine):
        """Create a subclass of NotificationsEngine without implementing the abstract method."""

        def __init__(self, logger: logging.Logger | None = None):
            self.logger = logger or logging.getLogger(__name__)
            # Override the constructor to avoid calling the abstract method

        def load_config(self):
            """Calls the abstract method to trigger NotImplementedError"""
            super().load_config()

        def validate_config(self):
            """Calls the abstract method to trigger NotImplementedError"""
            super().validate_config()

        def send_notifications(self, message: str):
            """Calls the abstract method to trigger NotImplementedError"""
            super().send_notifications(message=message)

        def init_error(self):
            """Calls the constructor method to trigger NotImplementedError"""
            super().__init__()

    # Instantiate the subclass and mock the logger to prevent actual logging
    engine = TestNotificationsEngine(logger=mock.MagicMock())

    # Check that calling the method raises NotImplementedError
    with pytest.raises(NotImplementedError):
        engine.load_config()
    with pytest.raises(NotImplementedError):
        engine.validate_config()
    with pytest.raises(NotImplementedError):
        engine.send_notifications(message="")
    with pytest.raises(NotImplementedError):
        engine.init_error()


def test_load_config_called(mock_notifications_engine):
    """Test that load_config is called during initialization."""
    with mock.patch.object(mock_notifications_engine, "load_config", autospec=True) as mock_load_config:
        mock_notifications_engine.load_config()
        mock_load_config.assert_called_once()


def test_validate_config_called(mock_notifications_engine):
    """Test that validate_config is called during initialization."""
    with mock.patch.object(mock_notifications_engine, "validate_config", autospec=True) as mock_validate_config:
        mock_notifications_engine.validate_config()
        mock_validate_config.assert_called_once()


def test_send_notifications_called(mock_notifications_engine):
    """Test that send_notifications is called during initialization."""
    with mock.patch.object(mock_notifications_engine, "send_notifications", autospec=True) as mock_send_notifications:
        mock_notifications_engine.send_notifications(mock.Mock())
        mock_send_notifications.assert_called_once()
