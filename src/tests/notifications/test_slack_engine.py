"""
SlackEngine Test Suite
"""

import logging
from unittest.mock import Mock, patch

import pytest

from notifications import SlackEngine  # Replace 'your_module' with the actual module name


@pytest.fixture
def mock_slack_engine():
    """Fixture to provide a mock SlackEngine instance."""
    with patch.dict(
        "os.environ", {"SLACK_WEBHOOK": "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"}
    ):
        with patch(
            "notifications.slack.WebhookClient"
        ) as mock_webhook_client:  # Replace 'your_module' with the actual module name
            mock_logger = Mock(spec=logging.Logger)
            mock_slack_engine = SlackEngine(logger=mock_logger)
            yield mock_slack_engine, mock_webhook_client, mock_logger


def test_init(mock_slack_engine):
    """Test the initialization of SlackEngine."""
    slack_engine, mock_webhook_client, mock_logger = mock_slack_engine

    # Check that the logger is set correctly
    assert slack_engine.logger == mock_logger

    # Check that the config is loaded properly
    assert (
        slack_engine.config["SLACK_WEBHOOK"]
        == "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
    )

    # Verify that the WebhookClient was initialized with the correct URL
    mock_webhook_client.assert_called_once_with(slack_engine.config["SLACK_WEBHOOK"])


def test_load_config(mock_slack_engine):
    """Test that the SlackEngine configuration is loaded correctly."""
    slack_engine, _, mock_logger = mock_slack_engine

    slack_engine.load_config()

    # Verify that the configuration is loaded correctly from the environment variables
    assert (
        slack_engine.config["SLACK_WEBHOOK"]
        == "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
    )
    mock_logger.debug.assert_called_with(f"loaded_vars: {slack_engine.config}")


def test_load_config_failure():
    """Test that an error is logged when environment variables are not set correctly."""
    with patch.dict("os.environ", {}, clear=True):
        mock_logger = Mock(spec=logging.Logger)

        # Patch WebhookClient to prevent KeyError
        with patch("notifications.slack"):  # Replace 'your_module' with the actual module name
            with pytest.raises(KeyError):
                SlackEngine(logger=mock_logger)


def test_validate_config(mock_slack_engine):
    """Test the validate_config method."""
    slack_engine, mock_webhook_client, _ = mock_slack_engine
    mock_response = Mock()
    mock_response.status_code = 200
    mock_response.body = "ok"
    mock_webhook_client.return_value.send.return_value = mock_response

    slack_engine.validate_config()

    # Verify that the webhook's send method is called with the correct validation message
    mock_webhook_client.return_value.send.assert_called_once_with(text="SlackEngine validation message")


def test_send_notifications_success(mock_slack_engine):
    """Test sending notifications successfully."""
    slack_engine, mock_webhook_client, _ = mock_slack_engine
    mock_response = Mock()
    mock_response.status_code = 200
    mock_response.body = "ok"
    mock_webhook_client.return_value.send.return_value = mock_response

    slack_engine.send_notifications("Test message")

    # Verify that the webhook's send method is called with the correct message
    mock_webhook_client.return_value.send.assert_called_once_with(text="Test message")


def test_send_notifications_failure(mock_slack_engine):
    """Test sending notifications with failure response."""
    slack_engine, mock_webhook_client, _ = mock_slack_engine
    mock_response = Mock()
    mock_response.status_code = 500
    mock_response.body = "error"
    mock_webhook_client.return_value.send.return_value = mock_response

    with pytest.raises(AssertionError):
        slack_engine.send_notifications("Test message")

    # Verify that the webhook's send method is called with the correct message
    mock_webhook_client.return_value.send.assert_called_once_with(text="Test message")
