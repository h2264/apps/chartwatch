"""
GitOpsManager
"""

from __future__ import annotations

import logging

import yaml

from handlers import ChartHandler, GitHandler
from notifications import SlackEngine


class GitOpsManager:
    """Concrete class for managing GitOps actions."""

    def __init__(
        self: GitOpsManager, payload_yaml: yaml.YAMLObject | None, logger: logging.Logger | None = None
    ) -> None:
        """
        Initialize a GitOpsManager instance.

        Args:
            payload_yaml (yaml.YAMLObject | None): YAML object containing configuration data for GitOps.
            logger (logging.Logger | None, optional): Logger instance for logging operations.
            If not provided, a default logger is used.

        Sets up the RepositoryManager with the provided payload and initializes other components.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.git_handlers = {}
        self.chart_handlers = {}
        self.notification_engines = {}
        self.configure_repositories(payload_yaml=payload_yaml)
        self.load_notification_engines()
        self.upgradeable_charts = {}

    def configure_repositories(self: GitOpsManager, payload_yaml: yaml.YAMLObject) -> None:
        """Configure git and chart repositories from the payload YAML."""
        for repo_name, repo_config in payload_yaml["git_repositories"].items():
            self.git_handlers[repo_name] = ()
            self.configure_git_repositories(_repo_name=repo_name, _repo_config=repo_config)
            self.configure_chart_repositories(_repo_name=repo_name, git_handler=self.git_handlers[repo_name])

    def configure_git_repositories(self: GitOpsManager, _repo_name: str, _repo_config: dict) -> None:
        """Set up a GitHandler for a given repository."""
        try:
            self.logger.info(f"configuring {_repo_name} git repository [{_repo_config['url']}]")
            self.git_handlers[_repo_name] = GitHandler(
                _repo_name=_repo_name,
                _url=_repo_config["url"],
                _path=_repo_config["destination_path"],
                _app_files=_repo_config["chart_paths"],
            )
            self.logger.debug(f"declare chart handler for git repository {_repo_name}")
            self.chart_handlers[_repo_name] = {}
        except Exception as e:
            self.logger.error(f"Failed to construct {_repo_name} git repository: {e}")

    def configure_chart_repositories(self: GitOpsManager, _repo_name: str, git_handler: GitHandler) -> None:
        """Set up ChartHandlers for each application file in the repository."""
        for app_file, app_file_contents in git_handler.apps.items():
            self.chart_handlers[_repo_name][app_file] = None
            try:
                self.logger.info(f"configuring chart handler for file [{app_file}] from git repository [{_repo_name}]")
                self.chart_handlers[_repo_name][app_file] = ChartHandler(_app_file=app_file_contents)
            except Exception as e:
                self.logger.error(f"Failed to construct {_repo_name}/{app_file} chart handler: {e}")

    def sync_repositories(self: GitOpsManager) -> None:
        """Synchronize all git repositories and chart handlers."""
        for git_repository, git_handler in self.git_handlers.items():
            self.sync_git_handlers(_git_repository=git_repository, _git_handler=git_handler)
            for app_file, app_file_contents in git_handler.apps.items():
                self.sync_chart_handlers(
                    git_repository=git_repository, app_file=app_file, app_file_contents=app_file_contents
                )

    def sync_git_handlers(self: GitOpsManager, _git_repository: str, _git_handler: GitHandler) -> None:
        """Update a Git repository by pulling the latest changes."""
        self.logger.info(f"Updating git repository: {_git_repository}")
        _git_handler.pull_repository()

    def sync_chart_handlers(self: GitOpsManager, git_repository: str, app_file: str, app_file_contents: dict) -> None:
        """Update chart repositories for a given application file."""
        self.logger.debug(f"Updating chart handler {git_repository} for file {app_file}")
        self.chart_handlers[git_repository][app_file].load_apps(_app_file=app_file_contents)
        self.logger.debug("Updating chart repositories")
        self.chart_handlers[git_repository][app_file].update_chart_repos()
        self.logger.debug("Chart repositories updated")

    def sync_components(self: GitOpsManager) -> None:
        """
        Synchronize all components of the GitOpsManager.

        Calls methods to synchronize Git repositories and notification engines.
        """
        self.sync_repositories()
        self.sync_notification_engines()

    def load_notification_engines(self: GitOpsManager) -> None:
        """
        Load notification engines.

        Initializes the notification engines, currently only SlackEngine is supported.
        """
        self.notification_engines = {"slack": SlackEngine()}

    def sync_notification_engines(self) -> None:
        """
        Synchronize notification engines.

        Initializes the notification engines, currently only SlackEngine is supported.
        """
        for _, engine in self.notification_engines.items():
            engine.load_config()

    def build_notification_message(self: GitOpsManager) -> str:
        """
        Build notification string used for sending notifications.

        The notification string is constructed using a dict object provided by
        a ChartManager through get_updatable_charts().

        Args:
            logger (logging.Logger): logger object.
        Returns:
            notification_message (str): notification string containing the
                                        chart name, current and upstream version.
        """
        self.logger.info("building message")
        notification_message = ""
        print(f"test: {self.chart_handlers}")
        for repo_name, app_files in self.chart_handlers.items():
            self.logger.info(repo_name)
            for app_file, chart_handler in app_files.items():
                self.logger.info(app_file)
                notification_message += f"`{app_file}`\n"
                for chart_name, _ in chart_handler.charts.items():
                    self.logger.info(chart_name)
                    try:
                        local_version, remote_version, outdated = chart_handler.compare_version(chart_name=chart_name)
                        if outdated:
                            notification_message += f"Chart `{chart_name}` can be updated "
                            notification_message += f"from version `{local_version}` "
                            notification_message += f"to `{remote_version}`\n"
                    except Exception as e:
                        self.logger.error(f"Error during comparison: {e}")

        self.logger.debug(f"notification_message: {notification_message}")
        return notification_message

    def send_notifications(self: GitOpsManager) -> None:
        """
        Send notification through loadednotificationEngines.

        Notifications are built using build_notification_message

        Args:
            logger (logging.Logger): logger object.
        Returns:
            None.
        """
        notification_message = self.build_notification_message()
        self.logger.info(f"notification_engines: {self.notification_engines}")
        for engine_name, engine in self.notification_engines.items():
            self.logger.debug(f"engine_name: {engine_name}")
            engine.send_notifications(message=notification_message)

    def handle(self: GitOpsManager) -> None:
        """
        Handle GitOps actions.

        Sequence of operations are as follows:
        - Synchronise loaded git repository
        - Poll loaded chart repository and compare chart versions in git vs upstream
        - Send notifications concerning outdated charts
        Args:
            logger (logging.Logger): logger object.
        Returns:
            Nothing.
        """
        self.sync_components()
        self.send_notifications()

    def stop(self: GitOpsManager, signum: int):
        """Stop all tasks and perform teardown."""
        self.logger.info(f"Stopping the GitOpsManager due to signal {signum}")
        self.teardown()

    def teardown(self: GitOpsManager) -> None:
        """
        Clean up environment

        Deletes loaded helm chart repositories and cloned git repository.

        Args:
            logger (logging.Logger): logger object.
        Returns:
            None.
        """
        self.logger.warning("Teardown process initiated")
        for git_repository, git_handler in self.git_handlers.items():
            self.logger.warning(f"Teardown repository: {git_repository}")
            for app_file in git_handler.app_files:
                self.logger.warning(f"Teardown charts from file: {app_file}")
                self.chart_handlers[git_repository][app_file].delete_chart_repos()

            git_handler.delete_repository()
        self.logger.info("Teardown complete")
