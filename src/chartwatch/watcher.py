"""
Module initialisation for the Domain watcher of dnsfix
"""

from __future__ import annotations

import logging
from threading import Event

import yaml

from .gitops import GitOpsManager


class Watcher:
    """Runs the primary process that watches for listed domains."""

    def __init__(
        self: Watcher, polling_rate: float = 1.0, payload_yaml_path: str = "", logger: logging.Logger | None = None
    ) -> None:
        """Initialise the watcher."""
        self.logger = logger or logging.getLogger(__name__)
        self.payload_yaml = self.load_yaml(payload_yaml_path=payload_yaml_path)
        self.gmanager = GitOpsManager(payload_yaml=self.payload_yaml)
        self.polling_rate = polling_rate
        self.watch = False
        self.shutdown_event = Event()

    def start(self: Watcher):
        """Start the watcher."""
        self.logger.info("starting the watcher")
        self.watch = True

        while self.watch and not self.shutdown_event.is_set():
            try:
                self.gmanager.handle()
                self.logger.info(f"Polling rate: {self.polling_rate}")
                self.shutdown_event.wait(self.polling_rate)
            except Exception as e:
                self.logger.error(f"Error during handling: {e}")
                self.stop()

    def stop(self, signum=None, frame=None):
        """Stop the watcher."""
        self.logger.warning(f"Stopping the watcher due to signal {signum}")
        self.logger.debug(f"frame: {frame}")
        self.watch = False
        self.shutdown_event.set()  # Unblock waiting threads
        self.gmanager.stop(signum=signum)
        self.logger.info("Watcher has stopped.")

    def load_yaml(self: Watcher, payload_yaml_path: str = "") -> yaml.YAMLObject:
        """
        Load payload yaml.

        Args:
            payload_yaml_path (str): Absolute path of payload_yaml as string.
            logger (logging.Logger): logger object.
        Returns:
            yaml_data (yaml.YAMLObject): with loaded contents of payload_yaml.
        """
        with open(payload_yaml_path, "r", encoding="utf-8") as file:
            yaml_data = yaml.safe_load(file)

        self.logger.debug(f"yaml_data:\n{yaml.dump(yaml_data)}")
        return yaml_data
