ARG BASE_IMG="python:3.12.2-slim-bullseye"
FROM ${BASE_IMG} AS base

# Initialise test environment
ARG DEBIAN_FRONTEND=noninteractive
ARG BUILD_UID=1000
ARG BUILD_GID=1000
ARG USER=chartwatch

# Prepare base environment
RUN useradd $USER -b /home/${USER} -u ${BUILD_UID}
COPY --chown=${BUILD_UID}:${BUILD_GID} . /app
WORKDIR /app

# Prepare test environment and run tests
RUN apt update -y \
    && apt install -y make \
    && python3 -m venv .venv \
    && pip install poetry \
    && poetry install \
    && pip list \
    && make python-lint \
    && rm -rf .venv

# Prepare core environment
RUN python3 -m venv .venv \
    && pip install poetry \
    && poetry install --without dev \
    && pip list \
    && poetry run python -m chartwatch -h

FROM ${BASE_IMG} AS runtime
ARG RUN_UID=1000
ARG RUN_GID=1000
ARG USER=chartwatch
COPY --chown=${RUN_UID}:${RUN_GID} --from=base /app /app
WORKDIR /app

# Update, install git, remove apt cache, and run git -h
RUN apt-get update \
    && apt-get install -y curl git \
    && git --help

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh

# CLEANUP
RUN apt remove -y curl \
    && rm -rf /var/lib/apt/lists/*

RUN pip install poetry \
    && poetry run python -m chartwatch -h
